# OpenCV Test

Finally a platform agnostic working example.

## How to use

Clone the repo
```bash
  git clone git@gitlab.com:Cir0X/OpenCVTest.git    
```

Run it

```bash
  ./gradlew run
```
